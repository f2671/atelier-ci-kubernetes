# Automatisation du déploiement d'application sur Kubernetes

Le principe des charts Helm est de permettre de packager une application avec toutes ces briques applicatives, réseau interne pour l'installer en un appel API a un cluster Kubernetes.

```
helm install --generate-name <chart>
```

L'objectif de cette partie est de fournir de quoi livrer en continu un chart, ce qui permettra de livrer un binaire installable sur tout cluster kube existant.
## Packaging Helm
<img src="https://razorops.com/images/blog/helm-3-tree.png"/>

Pour créer un paquet "chart", on execute la commande :
```
helm package
```

Les charts regroupent tous les fichiers de configuration définis au préalable dans un zip.

Ces paquets sont utilisables en soit : 

```
 helm install mytomcat ./tomcat-1.2.3.tgz
```

## Dépots Helm

Helm a lui aussi un système de dépot, c'est un gestionnaire de paquet. 

https://fr.wikipedia.org/wiki/Gestionnaire_de_paquets

On peut ajouter des dépots que l'on n'a pas par défaut par la commande :
```
helm repo add <depot> <url-depot>
```

Il faut ensuite être a jour sur ce que l'on voit du dépôt.
```
helm repo update
```

Et l'on peut enfin parcourir les dépôts a la recherche du paquets que l'on souhaiterait utiliser.
```
helm search repo
```

## Dépots Helm exemple


Nous allons maintenant envisager d'utiliser un dépot helm : le dépôt bitnami par exemple. 
```
helm repo add bitnami https://charts.bitnami.com/bitnami
```

Ensuite il faut mettre a jour le dépôt.
```
helm repo update
```
On va chercher a installer un postgres déjà packagé :
```
helm search repo postgres
```
Et on l'installe sur le cluster en une ligne : 
```
helm install monpostgres bitnami/postgresql
```

## Dépot de fichier : Release

- A la main : dépot de fichier

- Via curl : sur l'api harbor https://${REGISTRY_URL}/api/chartrepo/${project}/charts

> Exemple de source

https://ruzickap.github.io/k8s-harbor/part-06/#upload-signed-helm-chart-using-cli




## [Retour a la page principale ↩️ ](../../README.md)
