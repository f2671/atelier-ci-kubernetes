# Integration continue dans Gitlab

<img src="https://miro.medium.com/max/1400/1*fuvJn5MgjGfMpOcHfvw_MA.png"/>

Dans le cas de Gitlab, l'intégration continue se concentre autour d'un fichier : le `.gitlab-ci.yml` .

Ce fichier permet la définition d'étapes (`steps`), qui permettent de gérer une arborescence de scripts (`jobs`) que l'on souhaite utiliser.

## Décortiquer un fichier exemple

```yaml
stages:
  - test
  - build
  - deploy

maven-test:
  stage: test
  script:
    - mvn test


build-maven:
  stage: build
  script:
    - mvn package -DskipTests=true
  artifacts:
    paths:
      - target/ROOT.war
    expire_in: 1 week

deploy-to-prod:
  stage: deploy
  script:
    - curl -T target/ROOT.war ftp://example.org
```
- 3 étapes : test, build, deploy
- On execute nos 3 scripts, test puis construction de war puis deploiement.
- On sauvegarde le war dans gitlab pour l'envoyer
## Runners Gitlab

Dans le cas de gitlab, on met a disposition des machines. On les appelle des "Runner" gitlab.

(https://docs.gitlab.com/runner/)

Actuellement les runners disponibles sont visibles dans vos projets dans :

- Settings/ CI-CD / Runners

Actuellement 2 types de runners existent sur le Gitlab INSEE: 
- Des VM au CEI avec java, maven et d'autres chose d'installées
- Des runners qui tournent sur le cluster Kubernetes Lab au CEI. Et qui permettent de lancer des images docker.

## Pratique : Cas d'une application Java - Automatisation

> Remarque : vous pouvez le faire avec vos projets également

- On désire donc ici repartir du projet java : https://github.com/Ragatzino/Initiation-docker-kubernetes
- L'héberger sur gitlab.insee.fr (git clone => git push sur gitlab)

Puis :
  - Faire un pipeline en 2 étapes : test, packaging.
  - Reprendre les scripts de la partie 1
  - Utiliser l'image docker insee interne qui à maven jdk11 d'installé : harbor.developpement.insee.fr/ci-images/java:11
> Vous pouvez en rajouter plus si vous voulez valider par exemple
```

## [Retour a la page principale ↩️ ](../../README.md)
