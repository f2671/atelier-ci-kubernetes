# Integration continue

L'objectif de cette partie est de réaliser des rappels autour de l'intégration continue.

L'intégration s'inscrit au coeur d'un processus technique inhérent au métier de développeur : l'automatisation via scripts.
## Cas d'usage
Différents cas d'usages peuvent être envisagés, en voici `quelques exemples`:
- Linting : Analyse statique du code et du respect des règles du projet
- Compilation : Vérifier que le code compile bien.
- Testing : Lancement d'une batterie de tests, potentiellement sur différents environnements cible.
- Packaging : Construction d'un livrable (jar,zip,war,tar.gz,..) a partir des sources.
- Release : Enregistrement du livrable dans un dépot, agnostique de l'environnement.
- Deploiement : Lancement / Mise a jour / Déploiement d'un service a partir d'une version du code.
## Automatisation et ligne de commande
Une fois qu'un script logique est construit, il peut être rendu industriel en y extrayant la partie automatisable : Et cela se traduit par un script 

Exemple : 
> Etape 1 : Je veux construire un war

> Etape 2 : Lancer eclipse puis, clic droit update maven projects, puis je clique droit maven clean dans l'onglet maven, puis je peux procéder a maven install. Ensuite je peux récupérer le war qui s'est construit dans /target/

Ici on voit qu'il y a une partie adhérente a eclipse par ex, il faut donc automatiser au mieux le fonctionnement en passant en ligne de commande.
> Etape 3 : Adaptation en script 
```bash
mvn clean install
cd ./target/
```

> Ici l'on voit que l'on souhaiterait les lancer de manière plus régulière afin de bénéficier d'un retour sur l'investissement que représentent les tests, l'élaboration des scripts.
## Execution des scripts de manière régulière : Scripter en continu

Et si l'on pouvait, récupérer ses scripts et les executer de manière régulière ? C'est l'idée de l'intégration continue.

On donne donc a disposition des machines physiques ou virtuelles qui peuvent donc executer ses scripts sur un code livré au préalable.

> C'est ici que l'on peut constater l'intêret d'associer au versionning les pratiques d'intégration continue et déploiement continue.

On étudiera le cas Gitlab dans les parties suivantes.
## Customisation des environnements de travail avec Docker

Pour des raisons de légéreté et de correspondance a l'usage, l'émergence de Docker a également apporté sont lot de points positifs pour l'intégration continue.

Les différents avantages : 
- Permettre de consommer moins de ressources que des VMs qui accueilleraient tous les processus
- Permettre de reproduire des environnements spécifiques de manière très contrôlée, permettant ainsi d'identifier des bugs spécifiques (ex : montée de version ou compatibilité descendante)

Docker joue donc un grand rôle dans l'execution de processus et de tâches "routines" qui permettent donc d'améliorer la qualité des livraisons et de sécuriser les déploiements.


## [Retour a la page principale ↩️ ](../../README.md)
