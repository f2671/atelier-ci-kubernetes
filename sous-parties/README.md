# Sous parties

Le support a été séparé en sous parties pour plusieurs raisons : 

- Permettre de clarifier et de ne pas surcharger un fichier README
- Favoriser une navigation 
- Permettre de donner les fichiers de configuration pour la sous partie, ce qui permet donc d'executer de son côté différentes commandes.

> Remarque : Les fichier seront stockés dans /sous-partie/$NOM_SOUS_PARTIE/assets/

## Sommaire

-  [integration continue](/sous-parties/ci/README.md)
-  [scripting](/sous-parties/scripting/README.md)
-  [integration continue avec gitlab](/sous-parties/ci-gitlab/README.md)
-  [build avec kaniko](/sous-parties/ci-image-docker/README.md)
-  [creation d'une version preview](/sous-parties/ci-chart-et-release/README.md)
