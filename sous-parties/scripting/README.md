# Scripting : Cas d'une appli java


Récupérons l'application java suivante : https://github.com/Ragatzino/Initiation-docker-kubernetes

Reprenons l'exemple des choses qu'on aimerait faire sur une application
[Linting, Compilation, Testing, Packaging, Release, Deploiement]

Remarque pour la suite, on supposera qu'on est dans le cas d'une application maven (il en serait de même pour gradle).


<details><summary>Rappel Optionnel Maven</summary>

### Intêret de Maven

- Génération des projets a partir d'archétypes
- Gestionnaire de paquets
- Compilation packaging
- Installation et utilisation de plugins
### Cycle de vie Maven

Le cycle de vie maven est le suivant : 


> validate - validate the project is correct and all necessary information is available

> compile - compile the source code of the project

> test - test the compiled source code using a suitable unit testing framework. These tests should not require the code be packaged or deployed

> package - take the compiled code and package it in its distributable format, such as a JAR.

> verify - run any checks on results of integration tests to ensure quality criteria are met

> install - install the package into the local repository, for use as a dependency in other projects locally

> deploy - done in the build environment, copies the final package to the remote repository for sharing with other developers and projects.

</details>

<br/>

### Analyse du statique du code  : Formattage et règles avec checkstyle https://github.com/checkstyle/checkstyle

En local :

```
mvn checkstyle:checkstyle
```

> Pour plus d'infos : https://www.baeldung.com/checkstyle-java


### Analyse du statique du code  : Analyse de la qualité du code, et detection des erreurs

- Trouvez des bugs et du code mal conçu avec sonarqube https://github.com/SonarSource/sonarqube

https://docs.sonarqube.org/latest/analysis/scan/sonarscanner-for-maven/

Il faut installer un plugin puis faire : 
```
mvn clean verify sonar:sonar -Dsonar.login=myAuthenticationToken
```

> C'est dans la doc Rubrique Analyzing



## Compilation : Vérifier que le code compile bien.

Pour le coup, maven propose déjà de compiler pour vous :
```
mvn compile 
mvn compiler:testCompile
```


- Testing : Lancement d'une batterie de tests, potentiellement sur différents environnements cible.

```
mvn test
```

> On pourra potentiellement par la suite envisager une config avec une base de données.

- Packaging : Construction d'un livrable (jar,zip,war,tar.gz,..) a partir des sources.


> Construction d'un war
```
mvn package
```

Mais également possiblement : 

> Construction d'une image docker
```
docker build -t image:tag -f .
```

> Construction d'un chart helm (tar.gz)
```
helm package chart/
```



## Release : Enregistrement du livrable dans un dépot
Cette partie concerne l'enregistrement du livrable dans un dépot agnostique de l'environnement de déploiement.

- Dépot du jar sur le nexus, avec un identifiant : numéro de version etc..

```
mvn wagon
```
ou simplement
```
curl -T <objet> <destination>
```

> C'est équivalent côté docker helm

- Dépot de l'image construite

```
docker push <image-avec-tag>
```

> Dans les faits et pour des raisons de sécurité et de performance, nous utiliserons kaniko pour les images docker par la suite.

- Dépot du chart helm construit

```
 curl -k -u "${HARBOR_USERNAME}:${HARBOR_PASSWORD}" -X POST "${CHART_REPO_URL}" -H "Content-Type:multipart/form-data" -F "chart=${CHART_TGZ};type=application/x-compressed-tar" -H "Accept:application/json"
```

- Deploiement : Lancement / Mise a jour / Déploiement d'un service a partir d'une version du code.

## [Retour a la page principale ↩️ ](../../README.md)
