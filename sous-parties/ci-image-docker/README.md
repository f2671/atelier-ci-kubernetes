# Automatisation du packaging d'image docker

L'objectif ici est d'automatiser le packaging d'image Docker.

## Un peu de contexte

<details><summary>Resume de docker</summary>
<img src="https://www.alsacreations.com/xmedia/doc/original/1633864998-docker-meme.jpg">
</details>

<br/>


Une fois que l'on a déjà fait :

```
docker build
docker push
```

Sur un poste de travail, on aimerait pouvoir l'automatiser dans un pipeline d'intégration continue.


C'est pas nécessairement autorisé de faire cela sur un cluster voici pourquoi :
- La technologie Docker était nouvelle et donc on avait tendance a minimiser les risques
- Les clusters Kubernetes utilisaient Dockerd, ils avaient donc le même daemon qu'en local et on pouvait donc utiliser docker en ligne de commande depuis l'intérieur d'un cluster kubernetes. (worker par ex)

> Pour info les besoins développeur ont fait que le sspcloud a réouvert une offre ubuntu avec docker in docker.


## Notion de registre d'image

Les conteneurs docker, une fois construits à un endroit peuvent être entreposés dans ce que l'on appelle un registre Docker. Sur le premier TP, on avait utilisé DockerHub. C'est toujours possible d'envisager déposer sur DockerHub mais c'est publique donc ce n'est envisageable que pour des projets **OpenSource**. (d'autres raisons peuvent également bloquer)

On veut donc pousser une image, mais vers où ?

L'INSEE n'a pour l'instant qu'un registre d'images docker interne, qui est celui du cluster Kubernetes Lab : 

Accessible pour les utilisateurs interne sur https://harbor.developpement.insee.fr/

Ce registre repose sur la technologie Harbor : https://goharbor.io/

Utilisation d'une image docker interne:

https://harbor.developpement.insee.fr/harbor/projects/29/repositories

```
kubectl run -it --tty --image=harbor.developpement.insee.fr/ci-images/java:11 pod -- /bin/bash
```

## Kaniko

Kaniko est une solution qui permet de créer des images dans des environnements de type Kubernetes ou dans des environnement d'intégration continue (gitlab, jenkins ..)

https://github.com/GoogleContainerTools/kaniko

Gitlab nous le conseille ici : https://docs.gitlab.com/ee/ci/docker/using_kaniko.html#building-a-docker-image-with-kaniko

Pour cette partie nous vous invitons a partir du projet d'application "fil rouge" proposée ici : https://github.com/Ragatzino/Initiation-docker-kubernetes

=> Intégrée en interne ici https://gitlab.insee.fr/exemples-kube-orleans/preview-app-tomcat

Nous vous proposons de construire cette image en interne.
1. Créez un dépot sur gitlab.insee.fr
2. Envoyez y le dépot : 
```
git remote remove origin
git remote add origin <votre depot>
git push --set-upstream origin main
```
3. Créez vous un espace sur harbor : https://harbor.developpement.insee.fr/
    1. Login via OIDC
    2. New Project

Scénario : 
- On désire créer une image docker taguée par le hash du commit 
- On désire que cette image sur le harbor
> Il faut donc qu'on s'y authentifie

Pour l'authentification, on s'appuiera sur l'utilisation de compte robot comme dans le projet : https://gitlab.insee.fr/exemples-kube-orleans/image-docker


## [Retour a la page principale ↩️ ](../../README.md)
