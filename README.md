# Atelier CI avec Kubernetes

Nous faisons cette session pour experimenter en interne sur l'utilisation de kubernetes et docker.

> Remarque : Pour le CI nous utiliserons ici Gitlab CI mais il en serait de meme pour d'autres solutions

## 1 - Principes d'automatisation et de CI

Cette partie présente certains principes d'intégration continue et déploiement continu. 

- [Accéder a la sous partie ▶️](sous-parties/ci/)

## 2 - Construction de scripts

Pour automatiser encore faut il savoir quoi automatiser, d'où la partie ou l'on reprend les besoins standards d'une application.

> Remarque: l'exemple présenté pratique est une application java mais il en serait de même pour une application R, Python, Javascript ou autre.

- [Accéder a la sous partie ▶️](sous-parties/scripting/)

## 3 - Automatisation via l'implémentation CI/CD de Gitlab

Gitlab propose une implémentation au travers de la configuration d'un fichier : le `.gitlab-ci.yml`.

L'objectif ici sera de reprendre nos scripts, de les associer au versionning de notre application et de séparer en blocs logiques ces scripts.

- [Accéder a la sous partie ▶️](sous-parties/ci-gitlab/)



## 4 - Automatisation avec gitlab : Construction d'image docker

Pour une application, qui a comme cible Kubernetes, il faut d'abord recenser et sauvegarder les images docker.

On peut construire de manière automatisée ces images dans docker en faisant du docker in docker.

Kubernetes n'accepte pas, dans des environnements respectables, de conteneurs avec des droits tres eleves tel qu'il le faudrait pour faire du Docker in Docker. (sécurité)

Une alternative est d'utiliser des outils dédié avec des droits moindre, comme [Kaniko](https://github.com/GoogleContainerTools/kaniko)


- [Accéder a la sous partie ▶️](sous-parties/ci-image-docker/)


## 5 - Automatisation avec gitlab : Creation d'une version "preview" avec Helm

Maintenant qu'on a le moyen de créer des images docker, on peut imaginer une série de traitement consistant en le déploiement continu d'une application dockerizée.

Au programme : 
  - Construction des images docker (build)
  - Construction d'un Tar.gz utilisant ces images (build)
  - Déploiment du chart Helm ainsi construit sur un registre (Release)
  - Demande de lancement, avec valorisation par un fichier de configuration **values.yaml** sur un cluster Kubernetes (Deploy)


- [Accéder à la sous partie ▶️](sous-parties/ci-chart-et-deploiement/)

